# azure-airflow-kubernetes

This is a project that contains source code required to provision an AKS cluster with Terraform and to install Airflow
on the AKS cluster using a Helm chart.

The blogpost with further details can be found [here](https://www.fpgmaas.com/blog/azure-airflow-kubernetes).

```
.
├── Dockerfile
├── README.md
├── airflow
│   ├── pv-logs.yaml
│   ├── pvc-logs.yaml
│   └── values.yaml
├── dags
│   ├── example.py
│   ├── paralell_dat.py
│   ├── user_processing.py
└── terraform
    ├── main.auto.tfvars
    ├── main.tf
    └── variables.tf
```


# How to start
## Create Infrastructure via Terraform

```bash
cd terraform
```

```bash
terraform init
terraform fmt
terraform validate
terraform plan
terraform apply
```
You should see something like:
```bash
    Apply complete! Resources: 7 added, 0 changed, 0 destroyed.
```

To get all information about infrastructure:
```bash
    terraform show
```

To destroy at the end:
```bash
    terraform destroy
```

### Connect to AKS cluster
Get credentials via:
```bash
    az aks get-credentials --resource-group dfine-nmoabc-101abcn2-rg --name airflowaks
```

If successful, you should see all the pods with:
```bash
    kubectl get pods -A
```

## Installing Airflow with Helm
The file `airflow/values.yaml`


### GitSync to synchronize DAG's
To enable synchronization between GitHub and Airflow using GitSync, we use a deploy key. To create a new deploy key, navigate to ~/.ssh and run (if not already done):

```bash
ssh-keygen -t rsa -b 4096 -C "your@email.com"
```

As the name, choose airflowsshkey, and do not set a password. Now, print the public key to the console:

```bash
cat ~/.ssh/airflowsshkey.pub
```

Add the public key as a deploy key to your GitHub repository *(Settings > Deploy Keys > Add deploy key)*.


## Create Namespace
```bash
    kubectl create namespace airflow
```

## Create Secret
```bash
kubectl create secret generic -n airflow airflow-git-ssh-secret --from-file=gitSshKey=$HOME/.ssh/airflowsshkey
```

## Installing Airflow
```bash
helm repo add apache-airflow https://airflow.apache.org
```

Then, install Airflow in the airflow namespace using our values.yaml file:

```bash
helm install airflow apache-airflow/airflow -n airflow -f airflow/values.yaml --debug
```

When that is finished, you can test our Airflow instance by port forwarding the service:

```bash
kubectl port-forward svc/<webserver-service-name> 8080:8080 -n airflow
```

Find `webserver-pod-name` via:
```bash
    kubectl get services -n airflow
```

If you are using a VM and are accessing it via an IDE, you need to expose the port to your local machine. In Visual Studio Code, next to Terminal, you'll find the tab 'PORTS' where you can enter the port `8080`.

Now test the UI via `localhost:8080`


## Setup Postgres connection in Airflow

Navigate to *Admin > Connections* and create a new connection with:
-   
- name `postgres`
- connection type `Postgres`, 
- host: `hostname` or `ip-address`
- login: postgres
- password: postgres
- port: 5432

Find the `hostname`/`ip-address` via:
```bash
kubectl get services -n <namespace>
```

Now you can use the `PostgresOperator` with `postgres_conn_id='postgres'`

## Explore PostgrSQL
    - username: postgres
    - password: postgres

### Forward Port

```bash
kubectl port-forward svc/<your-postgresql-service> 5432:5432 -n <namespace>
```

If service name unknown:
```bash
kubectl get services -n <namespace>
```

Expose port on local machine (e.g. via VSC Ports).

#### Find database name
```bash
psql -h 127.0.0.1 -p 5432 -U postgres -l
```

#### Connect to database
```bash
psql -h localhost -p 5432 -U postgres -d <database-name>
```