# Azure Kubernetes Service (AKS) Infrastructure with Terraform

This Terraform project sets up an Azure Kubernetes Service (AKS) cluster within a custom Virtual Network (VNet) and subnet in Azure. It's designed to provide a secure, scalable foundation for deploying containerized applications with Kubernetes on Azure.

## Overview

The configuration creates the following resources:

- **Resource Group**: A container for all related Azure resources.
- **Virtual Network (VNet)**: Provides network isolation and connectivity for your AKS cluster.
- **Subnet**: A dedicated subnet within the VNet for AKS nodes, configured with delegation for Azure Container Instances.
- **AKS Cluster**: A managed Kubernetes cluster for deploying and managing containerized applications.

## Prerequisites

Before you begin, ensure you have the following:

- An Azure account with an active subscription.
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli) installed and configured.
- [Terraform](https://www.terraform.io/downloads.html) installed.

## Configuration

1. **Azure Authentication**: Ensure you're logged into Azure CLI and set the correct subscription:

    ```bash
    az login
    az account set --subscription "<Your-Subscription-ID>"
    ```

2. **SSH Key**: Generate an SSH key pair for secure access to the AKS nodes:

    ```bash
    ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
    ```

    Note: Replace `your_email@example.com` with your actual email. The public key will be used in the Terraform configuration.

3. **Terraform Init**: Initialize the Terraform project:

    ```bash
    terraform init
    ```

4. **Terraform Plan**: Review the actions Terraform will perform:

    ```bash
    terraform plan
    ```

5. **Terraform Apply**: Create the resources in Azure:

    ```bash
    terraform apply
    ```

    Confirm the action when prompted by Terraform.

## Resources Created

- **Resource Group**: `airflow-resources` located in "Germany West Central".
- **VNet**: `airflowVNet` with an address space of `10.0.0.0/16`.
- **Subnet**: `akssubnet` within `airflowVNet`, address prefix `10.0.1.0/24`.
- **AKS Cluster**: `airflow` with a default node pool named `workers`. The node pool consists of 3 nodes of size `Standard_D2s_v3`, each with 20 GB OS disks.

## Security

- The AKS cluster nodes are accessible via SSH using the generated key pair. Ensure the private key is kept secure.
- Consider implementing [Azure RBAC](https://docs.microsoft.com/en-us/azure/role-based-access-control/) for fine-grained access control to the Kubernetes resources.

## Cleanup

To destroy the resources created by Terraform, run:

```bash
terraform destroy
