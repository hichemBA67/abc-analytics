variable "app_name" {
  type        = string
  description = "Name of the application"
  default     = "abc-analytics"
}

variable "location" {
  type        = string
  description = "Location for the resources"
  default     = "West Europe"
}

variable "resource_group_name" {
  type        = string
  description = "The name of the existing resource group"
  default     = "dfine-nmoabc-101abcn2-rg"
}
